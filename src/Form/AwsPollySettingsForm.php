<?php

namespace Drupal\aws_polly\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Manage module settings.
 *
 * @package Drupal\aws_polly\Form
 */
class AwsPollySettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'aws_polly_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'aws_polly.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('aws_polly.settings');

    $form['aws_access_key'] = [
      '#title' => $this->t('AWS Access Key'),
      '#type' => 'textfield',
      '#description' => $this->t("Enter AWS Access Key"),
      '#default_value' => $config->get('aws_access_key'),
      '#required' => TRUE,
    ];

    $form['aws_secret_key'] = [
      '#title' => $this->t('AWS Secret Key'),
      '#type' => 'textfield',
      '#description' => $this->t("Enter AWS Secret Key"),
      '#default_value' => $config->get('aws_secret_key'),
      '#required' => TRUE,
    ];

    $form['aws_region'] = [
      '#title' => $this->t('AWS Region'),
      '#type' => 'textfield',
      '#description' => $this->t('Enter your <a href="@aws-link">AWS Region code</a>', [
        '@aws-link' => 'https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/using-regions-availability-zones.html',
      ]
      ),
      '#default_value' => $config->get('aws_region'),
      '#required' => TRUE,
    ];

    $form['global'] = [
      '#type' => 'details',
      '#title' => $this->t('Global Settings'),
      '#description' => $this->t('Config applicable through out the site'),
      '#open' => TRUE,
    ];

    // @todo be more dynamic here.
    $voices = aws_polly_voices($options = ["language_code" => "it-IT")];

    $form['global']['voice_id'] = [
      '#title' => $this->t('Available Voices'),
      '#type' => 'select',
      '#default_value' => $config->get('voice_id'),
      '#options' => $voices,
      '#required' => TRUE,
    ];

    $form['global']['display'] = [
      '#title' => $this->t('Content Display'),
      '#type' => 'textfield',
      '#description' => $this->t("Content display machine name so that we can convert that view mode data in audio."),
      '#default_value' => $config->get('display') ?? 'teaser',
      '#required' => TRUE,
    ];

    $form['global']['audio_signature'] = [
      '#title' => $this->t('Audio signature'),
      '#type' => 'textfield',
      '#description' => $this->t("Type here the audio signature at the end of every mp3 file produced."),
      '#default_value' => $config->get('audio_signature'),
      '#required' => FALSE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('aws_polly.settings')
      ->set('aws_access_key', $form_state->getValue('aws_access_key'))
      ->set('aws_secret_key', $form_state->getValue('aws_secret_key'))
      ->set('aws_region', $form_state->getValue('aws_region'))
      ->set('voice_id', $form_state->getValue('voice_id'))
      ->set('display', $form_state->getValue('display'))
      ->set('audio_signature', $form_state->getValue('audio_signature'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
