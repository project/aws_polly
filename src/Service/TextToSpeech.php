<?php

namespace Drupal\aws_polly\Service;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Session\AccountProxy;
use Drupal\file\Entity\File;
use Aws\Polly\PollyClient;
use Aws\Credentials\Credentials;
use Aws\Sts\StsClient;
use Aws\Sts\Exception\StsException;

/**
 * Contains the TextToSpeech.
 */
class TextToSpeech {


  /**
   * The aws polly configuration object.
   *
   * @var Drupal\Core\Config\ImmutableConfig
   */
  public $config;

  /**
   * The aws polly client object.
   *
   * @var Aws\Polly\PollyClient
   */
  protected $client;

  /**
   * Drupal\Core\Session\AccountProxy definition.
   *
   * @var \Drupal\Core\Session\AccountProxy
   */
  protected $currentUser;


  /**
   * Aws\Credentials\Credentials definition.
   *
   * @var \Aws\Credentials\Credentials
   */
  protected $credentials;

  /**
   * Creates a config of aws_polly.settings.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Session\AccountProxy $current_user
   *   The current user object.
   */
  public function __construct(ConfigFactoryInterface $config_factory, AccountProxy $current_user) {

    $this->config = $config_factory->get('aws_polly.settings');
    $this->currentUser = $current_user;
    $this->credentials = new Credentials($this->config->get('aws_access_key'), $this->config->get('aws_secret_key'));

    $this->client = new PollyClient([
      'version'     => '2016-06-10',
      'credentials' => $this->credentials,
      'region'      => $this->config->get('aws_region'),
    ]);

  }

  /**
   * This function calls aws polly service and return audio file.
   *
   * @param array $parameters
   *   Parameters to pass aws polly service.
   *
   * @return blob
   *   Audio file object for given parameter.
   */
  public function getAudio(array $parameters = []) {
    $format = $parameters['format'] ?? 'mp3';
    $result = $this->client->synthesizeSpeech([
      'OutputFormat' => $format,
      'Text'         => $parameters['text'],
      'TextType'     => 'text',
      'VoiceId'      => $this->config->get('voice_id'),
    ]);
    $file = $result->get('AudioStream')->getContents();
    return $file;
  }

  /**
   * Function generates the audio file and returns file object.
   *
   * @param array $parameters
   *   Parameters pass to generate file.
   *
   * @return Drupal\file\Entity\File
   *   Returns drupal file object.
   */
  public function generateFile(array $parameters = []) {
    $content = $this->getAudio($parameters);
    $file = $this->getFile($parameters);
    $this->uploadFile($file, $content);
    return $file;
  }

  /**
   * This function gets file object from given parameters.
   *
   * @param array $parameters
   *   Paramters pass to get file object.
   * @param string $uri_schema
   *   Uri scheme to get file.
   *
   * @return Drupal\file\Entity\File
   *   Return drupal file object.
   *
   * @todo give to filename more info about content (ie: node title).
   */
  public function getFile(array $parameters = [], $uri_schema = "public://") {
    $format = $parameters['format'] ?? 'mp3';
    $fileExt = $format;
    $month = date('m');
    $year = date('Y');
    $folder = $year . "-" . $month;
    $random = rand(100, 999);
    $user_id = $this->currentUser->id();
    $path = 'polly/' . $folder . "/polly_" . $random . "_" . $user_id . "_" . time() . "." . $fileExt;
    $file = File::create([
      'uid' => $user_id,
      'filename' => basename($path),
      'uri' => $uri_schema . $path,
      'status' => 1,
    ]);
    $file->save();
    return $file;
  }

  /**
   * Function uploads the file from given content.
   *
   * @param Drupal\file\Entity\File $file
   *   The drupal File object.
   * @param string $content
   *   The audio file content from aws polly service.
   */
  public function uploadFile(File $file, string $content) {
    $dir = dirname($file->getFileUri());
    if (!file_exists($dir)) {
      mkdir($dir, 0770, TRUE);
    }
    file_put_contents($file->getFileUri(), $content);
  }

  /**
   * Check if the current credentials are valid.
   */
  public function checkAwsCredentials() {
    $valid = TRUE;

    $client = new StsClient([
      'credentials' => $this->credentials,
      'region' => ($this->config->get('aws_region') ?: 'us-east-1'),
      'version' => '2011-06-15',
    ]);

    try {
      $result = $client->getCallerIdentity();
    }
    catch (StsException $e) {
      $valid = FALSE;
    }

    return $valid;
  }

}
