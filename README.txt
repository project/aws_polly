AWS Polly
-----------
AWS Polly module will convert sentences to audio data using the AWS SDK PHP
library and attach it to in node field.

We recommend installing the module using the composer only so that the
dependent libraries will be installed.

composer require drupal/aws_polly

How to use
------------

1. Create fields for mp3 files (see head of the aws_polly.module file)
2. Create a separate display for audio convert or you can use any existing and
configure all fields which need to convert in the audio.
3. Please configure with AWS details and add display name in Content display
field on config URL. (/admin/config/system/aws_polly)
4. Enable aws polly for your content type
