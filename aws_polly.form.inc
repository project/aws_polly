<?php

/**
 * @file
 * Action on forms.
 */

use Drupal\Core\Form\FormStateInterface;

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Add checkbox to the content type form to enable/disable Polly
 * per content type. Add an option to enable aws_polly for this content type.
 */
function aws_polly_form_node_type_form_alter(array &$form, FormStateInterface $form_state) {

  $node_type = $form_state->getFormObject()->getEntity();

  $default = \Drupal::state()->get('aws_polly_enabled' . $node_type->id(), NULL);

  $form['workflow']['aws_polly_content_type'] = [
    '#type' => 'checkbox',
    '#title' => t('Enable AWS_Polly'),
    '#default_value' => $default,
    '#description' => t('Enable AWS Polly for this content type.'),
  ];

  $form['actions']['submit']['#submit'][] = 'aws_polly_form_node_type_submit';

  if (isset($form['actions']['save_continue'])) {
    $form['actions']['save_continue']['#submit'][] = 'aws_polly_form_node_type_submit';
  }

}

/**
 * Submit callback to store enable/disable aws_polly to node types.
 */
function aws_polly_form_node_type_submit(array $form, FormStateInterface $form_state) {
  $node_type = $form_state->getFormObject()->getEntity();
  \Drupal::state()->set('aws_polly_enabled' . $node_type->id(), $form_state->getValue('aws_polly_content_type'));
}
